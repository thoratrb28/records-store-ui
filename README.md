Records Store UI
================
This repository contains the UI part of 'Records Store'

## Used JavaScript framework
0. AngularJS
## Used Generator
0. Yeoman

## Used Task Runner
0. Gulp

## Table of contents

* [Installation](#Installation)
* [Run application](#Run)

## Installation


> These are global tools that has to be installed in order to run and build
Record Store application
**Tip:** For Ubuntu/Mac, use 'sudo' if your global npm folder needs root access.

0. Extract the Folder

0. NodeJS - Install latest stable version for your OS, https://nodejs.org/en/

0. Gulp - `npm install -g gulp`

0. Bower - `npm install -g bower`

## Run
>Fire below commands from the root location of the project folder

0. `npm install`

0. `bower install`

0. `gulp build`

0. `gulp serve`

