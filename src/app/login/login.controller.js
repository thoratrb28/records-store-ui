(function () {
  angular.module('recordsStoreUi')
    .controller('LoginController', ['$scope', 'LoginDM', '$state', 'User', function ($scope, LoginDM, $state, User) {
      var vm = this;
      vm.username = 'admin';
      vm.password = 'admin';

      /**
       *  Make user login request
       */
      vm.proceedLogin = function () {
        LoginDM.proceedLogin(vm.username, vm.password).then(function success(response) {
          if (!angular.isUndefined(response.isDBA)) {
            User.setUserName(angular.copy(vm.username));
            User.setIsDBA(angular.copy(response.isDBA));
            $state.go('home');
          }

        }, function error(rejection) {

        });
      }
    }])
})();
