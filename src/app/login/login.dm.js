(function(){
  'use strict';

  angular.module('recordsStoreUi')
    .factory('LoginDM', ['AppConstant', 'DataAPI', '$q', function (AppConstant, DataAPI, $q) {
      var methods = {};

        /**
         * Make Login request with formed request object
         *
         * @param username
         * @param password
         * @returns {Function} promise
         */
      methods.proceedLogin = function(username, password){
        var deferred = $q.defer();
        var config = {};

        config.url = AppConstant.URLS.BASE + AppConstant.URLS.LOGIN;
        config.method = AppConstant.HTTP_METHODS.POST;
        config.data = {
          userName: username,
          password: password
        };

        DataAPI.APICall(config).then(function success(response){
          deferred.resolve(response);
        }, function error(rejection){
          deferred.reject(rejection);
        });
        return deferred.promise;
      };

      return methods;
    }])
})();
