(function () {
  'use strict';

  angular
    .module('recordsStoreUi')
    .run(['$log', '$rootScope', '$state', '$window', 'Analytics', function ($log, $rootScope, $state, $window, Analytics) {

      Analytics.pageView();
      /*var googleTrackingId = 'UA-80131223-1';
      window._gaq = window._gaq || [];
      _gaq.push(['_setDomainName', 'none']);
      _gaq.push(['_setAccount', googleTrackingId]);*/


/*
      window._gaq = window._gaq || [];
      $window.ga('create', 'UA-80131223-1', 'auto');
      $rootScope.$on('$stateChangeSuccess', function (event) {
        console.log($window.ga);
        $window.ga('send', 'pageview', $location.path());
      });
*/


      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromSate, fromParams){
        console.log('user run: ', $window.localStorage.getItem('userName'));
        if(!$window.localStorage.getItem('userName') && toState.name != 'login'){
          event.preventDefault();
          $state.go('login', {reload: true});
        } else if($window.localStorage.getItem('userName') && toState.name == 'login'){
          event.preventDefault();
          $state.go('home', {reload: true});
        }
      });

      $log.debug('runBlock end');
    }]);
})();
