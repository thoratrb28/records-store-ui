(function() {
  'use strict';

  angular
    .module('recordsStoreUi', ['ui.router', 'ui.bootstrap', 'angular-google-analytics']);

})();
