(function () {
  'use strict';

  angular
    .module('recordsStoreUi')
    .controller('MainController', ['MainDM', '$uibModal', 'AppConstant', 'User', '$state', 'CommonUtil', 'Analytics', function (MainDM, $uibModal, AppConstant, User, $state, CommonUtil, Analytics) {
      var vm = this;
      vm.paginationOptions = {};
      vm.paginationOptions.totalItems = 5;
      vm.paginationOptions.currentPage = 1;

      vm.flags = {};
      vm.flags.sortByTitle_ascending = true;

      vm.search = '';
      vm.filter = '';
      vm.sort = '';

      Analytics.addPromo('PROMO_1234', 'Summer Sale', 'summer_banner2', 'banner_slot1');
      Analytics.pageView();
      
      /**
       * Init block
       */
      (function init() {
        var params = _getParamsConfig(vm.paginationOptions.currentPage, vm.search, vm.filter, vm.sort);
        loadRecords(params);

      })();


      /**
       * Load saved records
       */
      function loadRecords(params) {
        MainDM.loadRecords(params).then(function (response) {
          vm.paginationOptions.totalItems = response.meta.totalRecords;
          vm.records = response.data;
        }, function error(rejection) {

        })
      }


      /**
       * Add new record. Open modal for getting input
       */
      vm.addRecord = function () {
        console.log('open modal');
        var modalInstance = CommonUtil.openModal({}, AppConstant.MODAL_TITLE.INSERT);

        modalInstance.result.then(function success(response) {
          console.log('response: ', response);
          MainDM.insertRecord(response.record).then(function success(response) {
            //todo: make it efficient
            //vm.records.push(response);
            var params = _getParamsConfig(vm.paginationOptions.currentPage, vm.search, vm.filter, vm.sort);
            loadRecords(params);
          }, function error(rejection) {

          });
        }, function error(rejection) {
          console.log('rejection: ', rejection);
        })
      };


      /**
       * Update selected record
       * @param index
       */
      vm.update = function (index) {

        console.log('userName: ', User.getUserName());
        console.log('isDBA: ', User.getIsDBA());
        //var modalInstance = openModal(vm.records[index], AppConstant.MODAL_TITLE.UPDATE);
        var modalInstance = CommonUtil.openModal(vm.records[index], AppConstant.MODAL_TITLE.UPDATE);

        modalInstance.result.then(function success(response) {
          console.log('response: ', response);
          if (response.operation == 'delete') {
            MainDM.deleteRecord(response.record._id);
            vm.records.splice(index, 1);
            $state.go('home');
          } else {
            MainDM.updateRecord(response.record).then(function success(response) {
              console.log('record updated successfully');
            }, function error(rejection) {
              console.log('error in update');
            });
          }
        }, function error(rejection) {
          console.log('rejection: ', rejection);
        })
      };

      /**
       * Handle page change
       */
      vm.pageChanged = function () {
        vm.search = AppConstant.DEFAULT_SEARCH_VALUE;
        var params = _getParamsConfig(vm.paginationOptions.currentPage, vm.search, vm.filter, vm.sort);
        loadRecords(params);
      };


      /**
       * Search records
       */
      vm.searchRecords = function () {
        //Set to '1', cos we want first page of result
        vm.paginationOptions.currentPage = 1;
        var params = _getParamsConfig(vm.paginationOptions.currentPage, vm.search, vm.filter, vm.sort);
        loadRecords(params);
      };


      /**
       *  Generate parameters for each request
       *
       * @param currentPage
       * @param search
       * @param filter
       * @param sort
       * @param sortOrder
       */
      function _getParamsConfig(currentPage, search, filter, sort, sortOrder) {
        var params = {};
        params['page_no'] = currentPage || 1;
        search ? params.search = search : '';
        filter ? params.filter = filter : '';
        sort ? params.sort = sort : '';

        return params;
      }

      /**
       * Test console
       */
      vm.testConsole = function () {
        console.log('test console')
      };


      /**
       * Sort records by title
       */
      vm.sortByTitle = function () {
        vm.flags.sortByTitle_ascending = !vm.flags.sortByTitle_ascending;

        vm.sort = vm.flags.sortByTitle_ascending ? AppConstant.TITLE : '-' + AppConstant.TITLE;

        var params = _getParamsConfig(vm.paginationOptions.currentPage, vm.search, vm.filter, vm.sort);
        loadRecords(params);
      }
    }]);
})();
