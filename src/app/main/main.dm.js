(function () {
  'use strict';

  angular.module('recordsStoreUi')
    .factory('MainDM', ['AppConstant', 'DataAPI', '$q', 'CommonUtil', function (AppConstant, DataAPI, $q, CommonUtil) {
      var methods = {};

      /**
       * Load stored records
       * @returns {Function} promise
       */
      methods.loadRecords = function (params) {
        var deferred = $q.defer();
        var config = {};

        var url = AppConstant.URLS.BASE + AppConstant.URLS.RECORDS;
        config.method = AppConstant.HTTP_METHODS.GET;

        params['page_size'] = 10;
        config.url = CommonUtil.urlFactory(url, params);

        DataAPI.APICall(config).then(function success(response) {
          deferred.resolve(response);
        }, function error(rejection) {
          deferred.reject(rejection);
        });
        return deferred.promise;
      };

      /**
       * Deletes according to id
       * @param id
       * @returns {Function} promise
       */
      methods.deleteRecord = function (id) {
        var deferred = $q.defer();
        var config = {};
        config.url = AppConstant.URLS.BASE + AppConstant.URLS.RECORDS + "/" + id;
        config.method = AppConstant.HTTP_METHODS.DELETE;

        DataAPI.APICall(config).then(function success(response) {
          deferred.resolve(response);
        }, function error(rejection) {
          deferred.reject(rejection);
        });

        return deferred.promise;
      };

      /**
       * Inert new record
       * @param record
       * @returns {Function} promise
       */
      methods.insertRecord = function (record) {
        var deferred = $q.defer();
        var config = {};
        config.url = AppConstant.URLS.BASE + AppConstant.URLS.RECORDS;
        config.method = AppConstant.HTTP_METHODS.POST;
        config.data = record;

        DataAPI.APICall(config).then(function success(response) {
          deferred.resolve(response);
        }, function error(rejection) {
          deferred.reject(rejection);
        });
        return deferred.promise;
      };

      /**
       * Update existing record
       * @param record
       * @returns {Function} promise
         */
      methods.updateRecord = function (record) {
        var deferred = $q.defer();
        var config = {};
        config.url = AppConstant.URLS.BASE + AppConstant.URLS.RECORDS;
        config.method = AppConstant.HTTP_METHODS.PUT;
        config.data = record;

        DataAPI.APICall(config).then(function success(response) {
          deferred.resolve(response);
        }, function error(rejection) {
          deferred.reject(rejection);
        });
        return deferred.promise;
      };

      return methods;
    }])
})();
