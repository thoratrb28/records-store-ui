(function () {
  'use strict';

  angular
    .module('recordsStoreUi')
    .config(['$logProvider', 'AnalyticsProvider', function ($logProvider, AnalyticsProvider) {
      // Enable log
      $logProvider.debugEnabled(true);

      AnalyticsProvider
        .logAllCalls(true)
        .startOffline(true)
        .useECommerce(true, true);
      AnalyticsProvider.setAccount('UA-80131223-1');
      AnalyticsProvider.useDisplayFeatures(true);
      AnalyticsProvider.useCrossDomainLinker(true);

      // Track all routes (default is true).
      AnalyticsProvider.trackPages(true);

      // Track all URL query params (default is false).
      AnalyticsProvider.trackUrlParams(true);

      // Ignore first page view (default is false).
      // Helpful when using hashes and whenever your bounce rate looks obscenely low.
      AnalyticsProvider.ignoreFirstPageLoad(true);


      // Change the default page event name.
      // Helpful when using ui-router, which fires $stateChangeSuccess instead of $routeChangeSuccess.
      AnalyticsProvider.setPageEvent('$stateChangeSuccess');
      AnalyticsProvider.logAllCalls(true);

      console.log('config executed');
    }]);
})();
