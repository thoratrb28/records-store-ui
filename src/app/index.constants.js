/* global malarkey:false, moment:false */
(function () {
  'use strict';

  angular
    .module('recordsStoreUi')
    .constant('AppConstant', {
      HTTP_METHODS: {
        POST: 'POST',
        GET: 'GET',
        PUT: 'PUT',
        DELETE: 'DELETE'
      },
      URLS: {
        BASE: 'http://localhost:5000/',
        LOGIN: 'login',
        RECORDS: 'records',
        MODAL_TEMPLATE_URL: 'app/components/record-modal/record-modal.html'
      },
      CONTENT_TYPES: {
        APPLICATION_JSON: 'application/json',
        APPLICATION_X_WWW_FORM_URLENCODED: 'application/x-www-form-urlencoded'
      },
      MODAL_SIZE: {
        LARGE: 'lg',
        SMALL: 'sm'
      },
      MODAL_TITLE: {
        INSERT: 'Insert new Record',
        UPDATE: 'Update Record'
      },
      DEFAULT_SEARCH_VALUE: '',
      TITLE: 'title',
      SORT_ORDER: {
        ASCENDING: 'ascending',
        DESCENDING: 'descending'
      }
    });

})();
