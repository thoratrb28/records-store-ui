(function () {
  'use strict';

  angular
    .module('recordsStoreUi')

    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
      $stateProvider
        .state('home', {
          url: '/home',
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'main'
        })
        .state('login', {
          url: '/login',
          templateUrl: 'app/login/login.html',
          controller: 'LoginController',
          controllerAs: 'login'
        });
      $urlRouterProvider.otherwise('/login');
      $httpProvider.defaults.headers.common = {};
      $httpProvider.defaults.headers.post = {};
      $httpProvider.defaults.headers.put = {};
      $httpProvider.defaults.headers.options = {};
      $httpProvider.defaults.headers.patch = {};
      $httpProvider.defaults.headers.delete = {};

      $httpProvider.interceptors.push('HttpInterceptors');

    }]);

})();
