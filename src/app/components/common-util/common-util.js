(function () {
  'use strict';

  angular.module('recordsStoreUi')
    .factory('CommonUtil', ['$uibModal', 'AppConstant', function ($uibModal, AppConstant) {
      var methods = {};

      /**
       * Form url
       * @param url
       * @param paginationConfig
       * @returns {string|*}
       */
      methods.urlFactory = function (url, paginationConfig) {
        var parameters = '';
        angular.forEach(paginationConfig, function (value, key) {
          parameters += '&' + key + '=' + value;
        });
        parameters = parameters.substr(1);
        url += '?' + parameters;
        return url;
      };


      /**
       * Open modal for 'RecordModalController'
       * @param record
       * @param modalTitle
       * @returns {*}
         */
      methods.openModal = function(record, modalTitle) {
        var modalInstance = $uibModal.open({
          templateUrl: AppConstant.URLS.MODAL_TEMPLATE_URL,
          controller: 'RecordModalController',
          controllerAs: 'modal',
          size: AppConstant.MODAL_SIZE.LARGE,
          resolve: {
            record: record || {},
            modalTitle: {title: modalTitle}
          }
        });
        return modalInstance;
      }

      return methods;
    }]);
})();
