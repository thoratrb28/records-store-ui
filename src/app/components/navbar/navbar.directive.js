(function () {
  'use strict';

  angular
    .module('recordsStoreUi')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
        creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($state, User) {
      var vm = this;
      vm.logout = function () {
        User.cleanUser();
        $state.go('login');
      }
    }
  }

})();
