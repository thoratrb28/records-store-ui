(function () {
  angular.module('recordsStoreUi')
    .factory('HttpInterceptors', ['$q', 'AppConstant', '$injector', function ($q, AppConstant, $injector) {
      var methods = {};


      /**
       * Request interceptors
       * @param config
       */
      methods.request = function (config) {
        //todo: add request header etc here
        config.headers['content-type'] = AppConstant.CONTENT_TYPES.APPLICATION_X_WWW_FORM_URLENCODED;
        config.transformRequest = function (obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        };

        return config;
      };

      /**
       * Handle request error
       * @param rejection
       */
      methods.requestError = function (rejection) {
        return $q.reject(rejection);
      };

      /**
       * Response interceptors
       * @param response
       */
      methods.response = function (response) {
        return response;
      };

      /**
       * Handle response error
       * @param rejection
       */
      methods.responseError = function (rejection) {
        //todo: check error code here
        var _$state = $injector.get('$state');
        if (rejection.status == 401) {
          _$state.go('login');
        }
        return $q.reject(rejection);
      };

      return methods;
    }])
})();
