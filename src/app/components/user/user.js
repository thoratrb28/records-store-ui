(function () {
  angular.module('recordsStoreUi')
    .service('User', ['$window', function ($window) {

      /**
       * Set User name
       * @param userName
       */
      this.setUserName = function (userName) {
        $window.localStorage.setItem('userName', userName);
      };
      /**
       * Get User name
       * @returns {*}
       */
      this.getUserName = function () {
        return $window.localStorage.getItem('userName');
      };

      /**
       * Set isDBA
       * @param DBAStatus
       */
      this.setIsDBA = function (isDBA) {
        $window.localStorage.setItem('isDBA', isDBA);
      };
      /**
       * Get isDBA
       * @returns {boolean}
       */
      this.getIsDBA = function () {
        return $window.localStorage.getItem('isDBA');
      };

      /**
       * Clean user details
       */
      this.cleanUser = function () {
        $window.localStorage.removeItem('userName');
        $window.localStorage.removeItem('isDBA');
      };
    }]);
})();
