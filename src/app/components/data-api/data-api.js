(function () {
  angular.module('recordsStoreUi')
    .service('DataAPI', ['$http', '$q', function ($http, $q) {
      /**
       * Utility method for making API calls
       *
       * @param config
       * @returns {Function} promise
       * @constructor
         */
      this.APICall = function (config) {
        
        var deferred = $q.defer();
        $http(config).then(function success(response) {
          deferred.resolve(response.data);
        }, function error(rejection) {
          deferred.reject(rejection);
        });
        return deferred.promise;
      }
    }]);
})();
