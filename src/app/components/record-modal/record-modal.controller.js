(function () {
  angular.module('recordsStoreUi')
    .controller('RecordModalController', ['$uibModalInstance', 'record', 'modalTitle', 'User', function ($uibModalInstance, record, modalTitle, User) {
      var vm = this;
      vm.flags = {};
      var modalResult = {};

      var recordStatus = 'existing';
      vm.modalTitle = modalTitle.title;
      vm.record = record;
      if (angular.isUndefined(vm.record.title)) {
        recordStatus = 'new';
        vm.record = {};
        vm.record.title = '';
        vm.record.data = '';
      }

      vm.flags.deleteButtonFlag = recordStatus == 'existing' && User.getIsDBA();

      /**
       * Save records
       */
      vm.save = function () {
        modalResult.operation = recordStatus == 'new'? 'save' : 'update';
        modalResult.record = vm.record;
        $uibModalInstance.close(modalResult);
      };

      /**
       * Discard changes
       */
      vm.discard = function () {
        $uibModalInstance.close('discard');
      };

      /**
       * Delete Record
       */
      vm.delete = function () {
        modalResult.operation = 'delete';
        modalResult.record = vm.record;
        $uibModalInstance.close(modalResult);
      };

    }])
})();
